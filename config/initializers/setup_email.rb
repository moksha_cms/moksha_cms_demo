# frozen_string_literal: true

# settings for the mailing system
#------------------------------------------------------------------------------

ActionMailer::Base.smtp_settings = {
  #--- usually overridden per domain, but needed for exception notification
  address: Rails.application.config.smtp_address,
  port: Rails.application.config.smtp_port,
  domain: Rails.application.config.smtp_domain,
  user_name: Rails.application.config.smtp_user_name,
  password: Rails.application.config.smtp_password,
  authentication: Rails.application.config.smtp_authentication,
  enable_starttls_auto: Rails.application.config.smtp_enable_starttls_auto
}

class DevelopmentMailInterceptor
  def self.delivering_email(message)
    message.subject = "[DEV #{message.to}] #{message.subject}"
    message.to      = Rails.application.config.development_email
    message.cc      = ''
    message.bcc     = ''
  end
end

if Rails.env.development? || Rails.env.test? || Rails.env.production_local?
  ActionMailer::Base.register_interceptor(DevelopmentMailInterceptor)
end
