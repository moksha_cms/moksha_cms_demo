# frozen_string_literal: true

# Subclassed from DmCore::ApplicationController, which handles locale and other
# common base functions
#------------------------------------------------------------------------------
class ApplicationController < DmCore::ApplicationController
  protect_from_forgery with: :null_session, prepend: true

  before_action do
    Rack::MiniProfiler.authorize_request if current_user.try(:is_sysadmin?)
  end

  helper :all
  helper DmCms::RenderHelper
  helper DmCms::PagesHelper
end
