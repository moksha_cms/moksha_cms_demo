# frozen_string_literal: true

# configure ExceptionNotification gem
# This is required here since Rails.application.config gets loaded
# after the moksha_cms gem
#------------------------------------------------------------------------------
if (Rails.env.production? || Rails.env.staging?) && !Rails.application.credentials.sentry_dsn.present?
  notifier = ExceptionNotifier.registered_exception_notifier(:email)
  notifier.base_options[:sender_address] =
    %("#{Rails.application.config.app_name}" <exception.notifier@#{Rails.application.config.base_domain}>)
  notifier.base_options[:exception_recipients] = [Rails.application.config.exception_emails_to]
end
