# MokshaCMS

A Rails CMS that includes

- content management
- built-in forum
- event management
- newsletter management
- learning management

## Installation

### Install a sample site

Using an SQLite database:

```
rake db:reset             # drops and recreates DB, runs migration, loads seed data
rake dm_core:create_site  # creates a new site
rake load_theme           # if the account prefix is `local`, then load data for a sample site
```

Run `rails s` and visit http://127.0.0.1:3000/en/users/sign_in and login with your admin credentials. You will be shown the front page of the site.  Visit http://127.0.0.1:3000/en/admin for the admin dashboard.
