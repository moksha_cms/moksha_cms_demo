# frozen_string_literal: true

module DmCore
  class RegistrationsController < Devise::RegistrationsController
    include DmCore::Concerns::RegistrationsController

    protected

    # Redirect to the signup_success page
    #------------------------------------------------------------------------------
    def after_inactive_sign_up_path_for(_resource)
      dm_cms.showpage_path(locale: DmCore::Language.locale, slug: 'signup_success')
    end
  end
end
