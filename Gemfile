# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '>= 2.7.6'

#------------------------------------------------------------------------------

gem 'mysql2', '~> 0.5.4'
gem 'puma', '~> 5.6.5'
gem 'rails', '~> 7.0', '< 7.1'
gem 'sass-rails', '>= 6.0'

gem 'moksha_cms',       git: 'https://gitlab.com/moksha_cms/moksha_cms.git', branch: '7-0-stable'
gem 'themes_for_rails', git: 'https://github.com/digitalmoksha/themes_for_rails.git'

# personal development gems
# local_path = '~/dev/_gitlab/moksha_cms'
# gem 'moksha_cms',             path: "#{local_path}/moksha_cms"

gem 'mini_racer', '~> 0.6.3', platforms: :ruby

# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.4'

# now required if using sprockets
gem 'sprockets-rails'

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.11'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# To use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.18'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.13.0', require: false

gem 'coffee-rails', '~> 5.0' # Use CoffeeScript for .coffee assets and views

# profiling gems
gem 'flamegraph'
gem 'memory_profiler'
gem 'rack-mini-profiler'
gem 'stackprof'

gem 'jquery-rails'
gem 'jquery-ui-rails'

gem 'bootstrap-sass', '~> 3.4.1'
gem 'rack-attack'
gem 'whenever', '~> 1.0', require: false

#------------------------------------------------------------------------------

group :development, :test do
  gem 'pry-byebug'
  # gem 'pry-rails', '~> 0.3.9'
  # gem 'pry-shell', '~> 0.5.0'

  gem 'factory_bot_rails', '~> 6.2'
  gem 'mocha', '~> 1.15', require: false
  gem 'rspec-rails', '~> 5.1'
  gem 'terminal-notifier'

  # gem 'quiet_assets'    # remove some asset pipeline noise from dev log
  gem 'progress_bar', require: false

  gem 'rubocop', '~> 1.36'
  gem 'rubocop-rspec', '~> 2.13'

  gem 'ruby-prof'
end

group :development, :production_local do
  gem 'better_errors', '~> 2.9'
  gem 'binding_of_caller'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen'
  gem 'web-console', '>= 4.2'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.1'

  # gem 'thin' use the Thin webserver during development
  gem 'capistrano', '~> 3.17'
  gem 'capistrano3-delayed-job', '~> 1.7'
  gem 'capistrano-rails', '~> 1.6'
  gem 'capistrano-rbenv', '~> 2.2'
  gem 'capistrano-rbenv-install'
  gem 'capistrano-yarn'
  # gem 'capistrano-puma'
  # gem 'capistrano-passenger'
  gem 'sshkit-interactive', require: false

  gem 'bundler-audit', '~> 0.9'
  gem 'letter_opener_web', '~> 2.0'
  gem 'tuttle', git: 'https://github.com/dgynn/tuttle', branch: 'develop'

  # performance gems - http://www.gregnavis.com/articles/5-gems-to-make-your-rails-app-faster/
  gem 'active_record_doctor'
  gem 'bullet' # small notification on every page that suffers from the N + 1 problem
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.37'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers', '~> 5.2'

  gem 'database_cleaner', '~> 2.0'
  gem 'faker', '~> 2.23'
  gem 'launchy', '~> 2.5'
  gem 'rspec-formatter-webkit'
  gem 'sqlite3'
  gem 'syntax'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
