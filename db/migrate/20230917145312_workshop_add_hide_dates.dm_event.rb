# This migration comes from dm_event (originally 20190629193643)
class WorkshopAddHideDates < ActiveRecord::Migration[5.0]
  def change
    add_column :ems_workshops, :hide_dates, :boolean, default: false
  end
end
