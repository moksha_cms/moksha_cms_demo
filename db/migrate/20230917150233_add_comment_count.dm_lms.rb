# This migration comes from dm_lms (originally 20130416223957)
class AddCommentCount < ActiveRecord::Migration[4.2]
  def change
    add_column    :lms_lesson_pages,  :comments_count, :integer, :default => 0
  end
end
