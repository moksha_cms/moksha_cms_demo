# This migration comes from dm_event (originally 20170613110938)
class AddWriteOff < ActiveRecord::Migration[5.0]
  def change
    add_column :ems_registrations, :writeoff, :boolean, default: false
  end
end
